<?php

    namespace App\Http\Controllers;

    use App\Models\Post;
    use App\Models\PostComment;
    use App\Models\PostLike;
    use App\Models\User;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;

    class PostController extends Controller
    {
        public function create()
        {
            if (Auth::user()) {
                return view("posts.create");
            } else {
                return redirect("/login");
            }
        } // end of create

        public function store(Request $request)
        {
            if (Auth::user()) {
                $post = new Post();

                $post->content = $request->input("content");
                $post->user_id = (Auth::user()->id);
                $post->save();

                return redirect("/home");
            } else {
                return redirect("/login");
            }
        } // end of store

        public function home()
        {
            if (Auth::user()) {
                $user = Auth::user();
                $posts = Post::where("is_active", true)->orderBy('created_at', 'desc')->get();
                $recentProfiles = User::where('id', '!=', $user->id)
                    ->orderBy('created_at', 'desc')
                    ->take(3)
                    ->get();

                return view("posts.home")->with('posts', $posts)->with('user', $user)->with('recentProfiles', $recentProfiles);
            } else {
                return redirect("/login");
            }

        } // end of home

        public function show($postId)
        {
            $currentUser = Auth::user();
            $post = Post::find($postId);
            $comments = PostComment::with('user')->where('post_id', $postId)->get();

            return view('posts.show')->with([
                'post' => $post,
                'comments' => $comments,
                'currentUser' => $currentUser
            ]);
        } // end of show

        public function myPosts()
        {
            if (Auth::user()) {
                $user = Auth::user();
                $posts = Auth::user()->posts()->where("is_active", true)->orderBy('created_at', 'desc')->get();
                $recentProfiles = User::where('id', '!=', $user->id)
                    ->orderBy('created_at', 'desc')
                    ->take(3)
                    ->get();

                return view('posts.home')->with('posts', $posts)->with('user', $user)->with('recentProfiles', $recentProfiles);
            } else {
                return redirect('/login');
            }
        } // end of myPosts

        public function like($postId)
        {
            $post = Post::find($postId);
            $userId = Auth::user()->id;

            // check if a post like has been made by this user before
            if ($post->likes->contains('user_id', $userId)) {
                // delete the like made by this user to unlike this post
                PostLike::where('post_id', $post->id)->where('user_id', $userId)->delete();
            } else {
                // User hasn't liked the post, create a new like
                $postLike = new PostLike;

                //define the properties of the $postLike object
                $postLike->post_id = $post->id;
                $postLike->user_id = $userId;

                //save this postLike object in the database
                $postLike->save();
            }
            return redirect("/posts/$postId"); // default return statement
        } // end of like

        public function comment($postId, Request $request)
        {
            $post = Post::find($postId);
            $userId = Auth::user()->id;

            $comment = new PostComment;
            $comment->content = $request->input('comment-text');
            $comment->post_id = $post->id;
            $comment->user_id = $userId;

            $comment->save();

            return redirect("/posts/$postId");
        } // end of comment

        public function updatePost($postId, Request $request)
        {
            $post = Post::find($postId);

            $post->content = $request->input('updated-post-content');

            $post->save();

            return redirect("/posts/$postId");
        }

        public function deletePost($postId)
        {
            // Find the post by its ID
            $post = Post::find($postId);

            // Delete the post
            $post->likes()->delete(); // This will delete all likes associated with the post
            $post->comments()->delete(); // This will delete all comments associated with the post
            $post->delete(); // This will delete the post itself

            return redirect("/home");
        }

        public function updateComment($postId, $commentId, Request $request)
        {
            $comment = PostComment::find($commentId);

            $comment->content = $request->input('updated-comment-content');

            $comment->save();

            return redirect("/posts/$postId");
        }

        public function deleteComment($commentId)
        {
            $comment = PostComment::find($commentId);

            $comment->delete();

            return redirect("/posts/$comment->post_id");
        }

        public function profile()
        {
            if (Auth::user()) {
                $currentUser = Auth::user();

                return view('profile')->with('currentUser', $currentUser);
            } else {
                return redirect("/login");
            }

        }

        public function updateAbout($profileId, Request $request)
        {
            $profile = User::find($profileId);

            $profile->about = $request->input('updated-profile-about');

            $profile->save();

            return redirect("/profile");
        }

        public function updatePlace($profileId, Request $request)
        {
            $profile = User::find($profileId);

            $profile->place = $request->input('updated-profile-place');

            $profile->save();

            return redirect("/profile");
        }

        public function updateBirthdate($profileId, Request $request)
        {
            $profile = User::find($profileId);

            $profile->birthdate = $request->input('updated-profile-birthdate');

            $profile->save();

            return redirect("/profile");
        }

        public function updateStatus($profileId, Request $request)
        {
            $profile = User::find($profileId);

            $profile->status = $request->input('updated-profile-status');

            $profile->save();

            return redirect("/profile");
        }


    } // end of PostController
