<?php

    use App\Http\Controllers\PostController;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Route;

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    Route::get('/', function () {
        return view('welcome');
    });

    Auth::routes();

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::get('/posts/create', [PostController::class, 'create']);

    Route::post('/posts', [PostController::class, 'store']);

    Route::get('/home', [PostController::class, 'home']);

    Route::get('/posts/{postId}', [PostController::class, 'show']);

    Route::get('/myPosts', [PostController::class, 'myPosts']);

    Route::put('/posts/{postId}/like', [PostController::class, 'like']);

    Route::post('/posts/{postId}/comment', [PostController::class, 'comment']);

    Route::put('/posts/{postId}/update-post', [PostController::class, 'updatePost']);

    Route::delete('posts/{postId}/delete-post', [PostController::class, 'deletePost']);

    Route::put('posts/{postId}/{commentId}/update-comment', [PostController::class, 'updateComment']);

    Route::delete('posts/{commentId}/delete-comment', [PostController::class, 'deleteComment']);

    Route::get('/profile', [PostController::class, 'profile']);

    Route::put('/profile/{profileId}/update-profile-about', [PostController::class, 'updateAbout']);

    Route::put('/profile/{profileId}/update-profile-birthdate', [PostController::class, 'updateBirthdate']);

    Route::put('/profile/{profileId}/update-profile-place', [PostController::class, 'updatePlace']);

    Route::put('/profile/{profileId}/update-profile-status', [PostController::class, 'updateStatus']);
