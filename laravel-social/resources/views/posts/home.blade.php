@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row justify-content-center">

            {{-- 1st column --}}
            <div class="col-12 col-md-12 col-lg-2 order-lg-1 order-1">
                <div class="card bg-white shadow rounded-0 border-0 text-center mb-2">
                    <ul class="list-group bg-white list-group-flush">
                        <li class="list-group-item bg-white p-3">
                            <div>
                                <img class="img-fluid rounded-circle me-2" alt="avatar1"
                                     src="{{ $user->image }}"
                                     style="width: 80px; height: 80px; object-fit: cover;"/>
                                <h5 class="mt-3">{{ $user->name }}</h5>
                            </div>
                        </li>
                        <li class="list-group-item bg-white">
                            <div class="d-flex flex-row justify-content-center">
                                <div class="flex-grow-1">
                                    <p class="text-muted mb-0">Posts</p>
                                    <p class="small fw-bold mb-0">{{ $user->posts->count() }}</p>
                                </div>

                                <div class="flex-grow-1">
                                    <p class="text-muted mb-0">Likes</p>
                                    <p class="small fw-bold mb-0">{{ $user->likes->count() }}</p>
                                </div>

                                <div class="flex-grow-1">
                                    <p class="text-muted mb-0 ">Comments</p>
                                    <p class="small fw-bold mb-0">{{ $user->comments->count() }}</p>
                                </div>
                            </div>
                        </li>

                        <li class="list-group-item bg-white">
                            <a class="text-decoration-none" href="/profile"><p class="text-primary small mb-0">Your
                                    Profile</p></a>
                        </li>
                    </ul>
                </div>
            </div>

            {{-- Create post --}}
            <div class="col-12 col-md-12 col-lg-6 order-lg-2 order-3">
                <div class="card bg-white shadow rounded-0 border-0 mb-2">
                    <div class="d-flex p-3">
                        <img class="img-fluid rounded-circle me-2" alt="avatar1"
                             src="{{ $user->image }}"
                             style="width: 40px; height: 40px; object-fit: cover;"/>
                        <form method="POST" action="/posts" class="w-100">
                            @csrf
                            <div class="form-group">
                                <textarea class="form-control" style="resize: none" placeholder="Share your thoughts..."
                                          id="content" name="content" rows="2"></textarea>
                            </div>

                            <div class="mt-2">
                                <button type="submit" class="btn btn-primary">Post</button>
                            </div>
                        </form>
                    </div>
                </div>

                {{-- Posts section --}}
                @if(isset($posts) && count($posts) > 0)
                    @foreach($posts as $post)
                        <div class="card bg-white shadow rounded-0 border-0 mb-2">
                            <div class="p-3">
                                <a href="/posts/{{ $post->id }}" class="text-decoration-none text-reset">
                                    <div class="card-body p-0">
                                        <div class="d-flex">
                                            <img class="img-fluid rounded-circle" alt="avatar1"
                                                 src="{{ $post->user->image }}"
                                                 style="width: 40px; height: 40px; object-fit: cover;"/>
                                            <div class="ms-1 mt-1">
                                                <h6 class="card-title m-0 fw-bold">{{ $post->user->name }}</h6>
                                                <p class="text-muted small m-0">{{ $post->created_at }}</p>
                                            </div>
                                        </div>

                                        <p class="card-text text-muted my-3">{{ $post->content }}</p>
                                    </div>
                                    <div class="card-body d-flex align-items-end p-0">
                                    <span class="text-muted">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12"
                                                 fill="currentColor"
                                                 class="bi bi-hand-thumbs-up-fill" viewBox="0 0 16 16">
                                            <path
                                                d="M6.956 1.745C7.021.81 7.908.087 8.864.325l.261.066c.463.116.874.456 1.012.965.22.816.533 2.511.062 4.51a10 10 0 0 1 .443-.051c.713-.065 1.669-.072 2.516.21.518.173.994.681 1.2 1.273.184.532.16 1.162-.234 1.733q.086.18.138.363c.077.27.113.567.113.856s-.036.586-.113.856c-.039.135-.09.273-.16.404.169.387.107.819-.003 1.148a3.2 3.2 0 0 1-.488.901c.054.152.076.312.076.465 0 .305-.089.625-.253.912C13.1 15.522 12.437 16 11.5 16H8c-.605 0-1.07-.081-1.466-.218a4.8 4.8 0 0 1-.97-.484l-.048-.03c-.504-.307-.999-.609-2.068-.722C2.682 14.464 2 13.846 2 13V9c0-.85.685-1.432 1.357-1.615.849-.232 1.574-.787 2.132-1.41.56-.627.914-1.28 1.039-1.639.199-.575.356-1.539.428-2.59z"/>
                                        </svg>
                                    </span>
                                        <p class="small text-muted mb-0 m-1 align-items-center">
                                            @if($post->likes->count() <= 1)
                                                Like
                                            @else
                                                Likes
                                            @endif
                                            ({{ $post->likes->count() }})
                                        </p>
                                        <span class="text-muted ms-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12"
                                             fill="currentColor" class="bi bi-chat-fill" viewBox="0 0 16 16">
                                            <path
                                                d="M8 15c4.418 0 8-3.134 8-7s-3.582-7-8-7-8 3.134-8 7c0 1.76.743 3.37 1.97 4.6-.097 1.016-.417 2.13-.771 2.966-.079.186.074.394.273.362 2.256-.37 3.597-.938 4.18-1.234A9 9 0 0 0 8 15"/>
                                        </svg>
                                    </span>
                                        <p class="small text-muted mb-0 m-1">
                                            @if($post->comments->count() <= 1)
                                                Comment
                                            @else
                                                Comments
                                            @endif
                                            ({{ $post->comments->count() }})
                                        </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

            {{-- 3rd column --}}
            <div class="col-12 col-md-12 col-lg-2 order-lg-3 order-2">
                <div class="card bg-white shadow rounded-0 border-0 text-center mb-2">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item bg-white p-2">
                            <p class="fw-bold mb-0 text-start">Recently registered</p>
                        </li>
                        @foreach($recentProfiles as $recentProfile)
                            <li class="list-group-item bg-white">
                                <div class="d-flex align-items-center">
                                    <a class="text-decoration-none" href="#profile">
                                        <img class="img-fluid rounded-circle" alt="avatar1"
                                             src="{{ $recentProfile->image }}"
                                             style="width: 40px; height: 40px; object-fit: cover;"/>
                                    </a>
                                    <a class="text-decoration-none" href="#profile">
                                        <p class="small text-muted mb-0 ms-2">{{ $recentProfile->name }}</p>
                                    </a>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

@endsection
