@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row justify-content-center">
            <div class="col col-md-6 col-lg-6">
                <div class="card bg-white shadow border-0 mb-3">
                    <div class="p-3 pb-2">
                        <div class="card-body p-0">
                            <div class="d-flex">
                                <img class="img-fluid rounded-circle" alt="avatar1"
                                     src="{{ $post->user->image }}"
                                     style="width: 40px; height: 40px; object-fit: cover;"/>
                                <div class="ms-1 mt-1">
                                    <h6 class="card-title m-0 fw-bold">{{ $post->user->name }}</h6>
                                    <p class="text-muted small m-0">{{ $post->created_at }}</p>
                                </div>

                                {{-- Option for edit and delete post --}}
                                @if($post->user->id == $currentUser->id)
                                    <div class="dropdown ms-auto">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                             fill="currentColor"
                                             class="bi bi-three-dots-vertical" type="button" data-bs-toggle="dropdown"
                                             aria-expanded="false" viewBox="0 0 16 16">
                                            <path
                                                d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0m0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0m0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0"/>
                                        </svg>

                                        <div class="dropdown-menu">
                                            <button class="dropdown-item small" type="button" data-bs-toggle="modal"
                                                    data-bs-target="#update-post-modal"><i
                                                    class="bi bi-pencil-square fa-fw pe-2"></i>Edit
                                            </button>

                                            <form class="mb-0" id="delete-form"
                                                  action="/posts/{{ $post->id }}/delete-post"
                                                  method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button class="dropdown-item small" type="submit"
                                                        onclick="confirmDelete()"><i
                                                        class="bi bi-trash fa-fw pe-2 mb-0"></i>Delete
                                                </button>
                                            </form>

                                            {{-- Confirmation dialog --}}
                                            <script>
                                                function confirmDelete() {
                                                    if (confirm("Are you sure you want to delete this post?")) {
                                                        document.getElementById('delete-form').submit();
                                                    } else {
                                                        event.preventDefault();
                                                    }
                                                }
                                            </script>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <h6 class="card-text text-muted my-3">{{ $post->content }}</h6>
                        </div>

                        {{-- Display count of likes and comments --}}
                        <div class="card-body d-flex align-items-center p-0">
                                    <span class="text-muted">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12"
                                                 fill="currentColor"
                                                 class="bi bi-hand-thumbs-up-fill" viewBox="0 0 16 16">
                                            <path
                                                d="M6.956 1.745C7.021.81 7.908.087 8.864.325l.261.066c.463.116.874.456 1.012.965.22.816.533 2.511.062 4.51a10 10 0 0 1 .443-.051c.713-.065 1.669-.072 2.516.21.518.173.994.681 1.2 1.273.184.532.16 1.162-.234 1.733q.086.18.138.363c.077.27.113.567.113.856s-.036.586-.113.856c-.039.135-.09.273-.16.404.169.387.107.819-.003 1.148a3.2 3.2 0 0 1-.488.901c.054.152.076.312.076.465 0 .305-.089.625-.253.912C13.1 15.522 12.437 16 11.5 16H8c-.605 0-1.07-.081-1.466-.218a4.8 4.8 0 0 1-.97-.484l-.048-.03c-.504-.307-.999-.609-2.068-.722C2.682 14.464 2 13.846 2 13V9c0-.85.685-1.432 1.357-1.615.849-.232 1.574-.787 2.132-1.41.56-.627.914-1.28 1.039-1.639.199-.575.356-1.539.428-2.59z"/>
                                        </svg>
                                    </span>
                            <p class="small text-muted mb-0 mx-1 align-items-center">
                                @if($post->likes->count() <= 1)
                                    Like
                                @else
                                    Likes
                                @endif
                                ({{ $post->likes->count() }})
                            </p>
                            <span class="text-muted ms-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12"
                                             fill="currentColor" class="bi bi-chat-fill" viewBox="0 0 16 16">
                                            <path
                                                d="M8 15c4.418 0 8-3.134 8-7s-3.582-7-8-7-8 3.134-8 7c0 1.76.743 3.37 1.97 4.6-.097 1.016-.417 2.13-.771 2.966-.079.186.074.394.273.362 2.256-.37 3.597-.938 4.18-1.234A9 9 0 0 0 8 15"/>
                                        </svg>
                                    </span>
                            <p class="small text-muted mb-0 mx-1">
                                @if($post->comments->count() <= 1)
                                    Comment
                                @else
                                    Comments
                                @endif
                                ({{ $post->comments->count() }})
                            </p>
                        </div>
                    </div>

                    {{-- Like and unlike section --}}
                    <div class="card-body px-3 py-1 border-top border-bottom">
                        <form class="d-flex align-items-center mb-0" method="POST" action="/posts/{{ $post->id }}/like">
                            @method('PUT')
                            @csrf
                            @if($post->likes->contains("user_id", Auth::id()))
                                <span class="text-primary">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12"
                                             fill="currentColor"
                                             class="bi bi-hand-thumbs-up-fill"
                                             viewBox="0 0 16 16">
                                        <path
                                            d="M6.956 1.745C7.021.81 7.908.087 8.864.325l.261.066c.463.116.874.456 1.012.965.22.816.533 2.511.062 4.51a10 10 0 0 1 .443-.051c.713-.065 1.669-.072 2.516.21.518.173.994.681 1.2 1.273.184.532.16 1.162-.234 1.733q.086.18.138.363c.077.27.113.567.113.856s-.036.586-.113.856c-.039.135-.09.273-.16.404.169.387.107.819-.003 1.148a3.2 3.2 0 0 1-.488.901c.054.152.076.312.076.465 0 .305-.089.625-.253.912C13.1 15.522 12.437 16 11.5 16H8c-.605 0-1.07-.081-1.466-.218a4.8 4.8 0 0 1-.97-.484l-.048-.03c-.504-.307-.999-.609-2.068-.722C2.682 14.464 2 13.846 2 13V9c0-.85.685-1.432 1.357-1.615.849-.232 1.574-.787 2.132-1.41.56-.627.914-1.28 1.039-1.639.199-.575.356-1.539.428-2.59z"/>
                                    </svg>
                                </span>

                                <input type="submit"
                                       class="text-primary btn btn-link btn-sm text-decoration-none small p-0 ms-1"
                                       value="Unlike">
                            @else
                                <span class="text-muted">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12"
                                             fill="currentColor"
                                             class="bi bi-hand-thumbs-up-fill" viewBox="0 0 16 16">
                                        <path
                                            d="M6.956 1.745C7.021.81 7.908.087 8.864.325l.261.066c.463.116.874.456 1.012.965.22.816.533 2.511.062 4.51a10 10 0 0 1 .443-.051c.713-.065 1.669-.072 2.516.21.518.173.994.681 1.2 1.273.184.532.16 1.162-.234 1.733q.086.18.138.363c.077.27.113.567.113.856s-.036.586-.113.856c-.039.135-.09.273-.16.404.169.387.107.819-.003 1.148a3.2 3.2 0 0 1-.488.901c.054.152.076.312.076.465 0 .305-.089.625-.253.912C13.1 15.522 12.437 16 11.5 16H8c-.605 0-1.07-.081-1.466-.218a4.8 4.8 0 0 1-.97-.484l-.048-.03c-.504-.307-.999-.609-2.068-.722C2.682 14.464 2 13.846 2 13V9c0-.85.685-1.432 1.357-1.615.849-.232 1.574-.787 2.132-1.41.56-.627.914-1.28 1.039-1.639.199-.575.356-1.539.428-2.59z"/>
                                    </svg>
                                </span>

                                <input type="submit"
                                       class="text-muted btn btn-link btn-sm text-decoration-none small p-0 ms-1"
                                       value="Like">
                            @endif
                        </form>
                    </div>

                    {{-- Display comments --}}
                    @if(isset($comments) && count($comments) > 0)
                        @foreach($comments as $comment)
                            <div class="card-body border-bottom px-3">
                                <div>
                                    <div class="d-flex">
                                        <img class="img-fluid rounded-circle" alt="avatar1"
                                             src="{{ $comment->user->image }}"
                                             style="width: 40px; height: 40px; object-fit: cover;"/>
                                        <div class="ms-1 mt-1">
                                            <h6 class="card-title m-0 fw-bold">{{ $comment->user->name }}</h6>
                                            <p class="text-muted small m-0">{{ $comment->created_at }}</p>
                                        </div>

                                        {{-- Option for edit and delete comment --}}
                                        @if($comment->user->id == $currentUser->id)
                                            <div class="dropdown ms-auto">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                     fill="currentColor"
                                                     class="bi bi-three-dots-vertical" type="button"
                                                     data-bs-toggle="dropdown"
                                                     aria-expanded="false" viewBox="0 0 16 16">
                                                    <path
                                                        d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0m0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0m0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0"/>
                                                </svg>

                                                <div class="dropdown-menu">
                                                    <button class="dropdown-item small edit-comment-btn" type="button"
                                                            data-bs-toggle="modal"
                                                            data-bs-target="#update-comment-modal"
                                                            data-comment-id="{{ $comment->id }}"
                                                            data-comment="{{ $comment->content }}">
                                                        <i
                                                            class="bi bi-pencil-square fa-fw pe-2"></i>Edit
                                                    </button>

                                                    <form id="delete-form"
                                                          action="/posts/{{ $comment->id }}/delete-comment"
                                                          method="POST" class="mb-0">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="dropdown-item small" type="submit"
                                                                onclick="confirmDelete()"><i
                                                                class="bi bi-trash fa-fw pe-2"></i>Delete
                                                        </button>
                                                    </form>

                                                    {{-- Confirmation dialog --}}
                                                    <script>
                                                        function confirmDelete() {
                                                            if (confirm("Are you sure you want to delete this post?")) {
                                                                document.getElementById('delete-form').submit();
                                                            } else {
                                                                event.preventDefault();
                                                            }
                                                        }
                                                    </script>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <h6 class="card-text text-muted mt-3">{{ $comment->content }}</h6>
                                </div>
                            </div>
                        @endforeach
                    @endif

                    {{-- Write a comment section --}}
                    <div class="card-body py-3">
                        <div class="d-flex">
                            <img class="img-fluid rounded-circle me-2" alt="avatar1"
                                 src="{{ $currentUser->image }}"
                                 style="width: 40px; height: 40px; object-fit: cover;"/>
                            <form method="POST" action="/posts/{{ $post->id }}/comment" class="w-100">
                                @csrf
                                <div class="form-group mb-2">
                                <textarea id="comment-text" style="resize: none" name="comment-text"
                                          placeholder="Write a comment"
                                          class="form-control" rows="1"></textarea>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-sm">Comment</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal for edit post --}}
    <div class="modal fade" id="update-post-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit post</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="POST" action="/posts/{{ $post->id }}/update-post" class="w-100">
                    @method('PUT')
                    @csrf
                    <div class="modal-body">
                        <div class="mb-3 form-group">
                            <textarea id="updated-post-content" name="updated-post-content"
                                      class="form-control"> {{ $post->content }}</textarea>
                        </div>
                    </div>
                    <div class="modal-footer form-group">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- Modal for edit comment --}}
    <div class="modal fade" id="update-comment-modal" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit comment</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="update-comment-form" method="POST"
                      action="" class="w-100">
                    @method('PUT')
                    @csrf
                    <div class="modal-body">
                        <div class="mb-3 form-group">
                            <textarea id="updated-comment-content" name="updated-comment-content"
                                      class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer form-group">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

<script>
    document.addEventListener('DOMContentLoaded', function () {
        // Listen for the modal to be shown
        document.getElementById('update-comment-modal').addEventListener('show.bs.modal', function (event) {
            console.log('Modal shown');

            // Get the button that triggered the modal
            const button = event.relatedTarget;
            console.log('Button:', button);

            // Extract comment id from data attribute
            let commentId = button.getAttribute('data-comment-id');

            // Update the form action with the comment id
            let form = document.getElementById('update-comment-form');
            form.action = "/posts/{{ $post->id }}/" + commentId + "/update-comment";

            // Extract comment content from data attribute
            const commentContent = button.getAttribute('data-comment');
            console.log('Comment content:', commentContent);

            // Set the value of the textarea in the modal
            document.getElementById('updated-comment-content').value = commentContent;
        });
    });
</script>
