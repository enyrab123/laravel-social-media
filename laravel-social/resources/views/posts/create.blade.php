@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-5">
                <form method="POST" action="/posts">
                    @csrf
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Share your thoughts..." id="content"
                               name="content">
                    </div>

                    <div class="mt-2">
                        <button type="submit" class="btn btn-success">Post</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

