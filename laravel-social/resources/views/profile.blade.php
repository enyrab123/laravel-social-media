@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row justify-content-center">
            {{-- 1st column --}}
            <div class="col-lg-7">
                {{-- Image section--}}
                <div class="card bg-white shadow rounded-0 border-0">
                    <div class="d-flex p-3 align-items-center">
                        <img class="img-fluid rounded-circle me-2" alt="avatar1"
                             src="{{ $currentUser->image }}"
                             style="width: 120px; height: 120px; object-fit: cover;"/>
                        <div>
                            <h5 class="fw-bold">{{ $currentUser->name }}</h5>
                            <p class="small text-muted mb-0">Joined date:</p>
                            <p class="small text-muted mb-0">{{ $currentUser->created_at }}</p>
                        </div>
                    </div>
                    {{-- Links section --}}
                    <div class="card-footer bg-white px-3 py-2">
                        <ul class="nav nav-bottom-line">
                            <li class="nav-item me-4 align-items-center">
                                <a class="nav-link p-0" href="#posts "><p class="text-muted mb-0">Posts</p></a>
                            </li>
                            <li class="nav-item me-4">
                                <a class="nav-link p-0" href="#about "><p class="text-muted mb-0">About</p></a>
                            </li>
                            <li class="nav-item me-4">
                                <a class="nav-link p-0" href="#friends "><p class="text-muted mb-0">Friends</p></a>
                            </li>
                        </ul>
                    </div>
                </div>

                {{-- Profile info --}}
                <div class="card bg-white shadow rounded-0 border-0 mt-2">
                    <div class="card-body p-3">
                        <h5 class="fw-bold">Profile Info</h5>

                        {{-- Overview --}}
                        <div class="rounded border px-3 py-2 mb-3">
                            <div class="d-flex align-items-center justify-content-between">
                                <h6>Overview</h6>
                                <div class="dropdown ms-auto">
                                    <!-- Card share action menu -->
                                    <a class="nav nav-link text-secondary mb-0" href="#" id="aboutAction"
                                       data-bs-toggle="dropdown" aria-expanded="false">
                                        <i class="bi bi-three-dots"></i>
                                    </a>
                                    <!-- Card share action dropdown menu -->
                                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="aboutAction" style="">
                                        <li><a class="dropdown-item small" data-bs-toggle="modal"
                                               data-bs-target="#update-about-modal"> <i
                                                    class="bi bi-pencil-square fa-fw pe-2"></i>Edit</a></li>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <p class="text-muted">
                                @if($currentUser->about === "")
                                    about not set
                                @else
                                    {{ $currentUser->about }}
                                @endif
                            </p>

                            {{-- Modal for edit about --}}
                            <div class="modal fade" id="update-about-modal" tabindex="-1"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Edit about</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                        </div>
                                        <form method="POST"
                                              action="/profile/{{ $currentUser->id }}/update-profile-about"
                                              class="w-100">
                                            @method('PUT')
                                            @csrf
                                            <div class="modal-body">
                                                <div class="mb-3 form-group">
                                                    <textarea id="updated-profile-about" name="updated-profile-about"
                                                              class="form-control"> {{ $currentUser->about }}</textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer form-group">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                                                    Close
                                                </button>
                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- Individual details --}}
                        <div class="row g-3">
                            <div class="col-sm-6">
                                <!-- Birthday START -->
                                <div class="d-flex align-items-center rounded border px-3 py-2">
                                    <!-- Date -->
                                    <p class="mb-0 text-muted">
                                        <i class="bi bi-calendar-date fa-fw me-2"></i> Born:
                                        <strong>
                                            @if($currentUser->birthdate === "")
                                                Birthdate not set
                                            @else
                                                {{ $currentUser->birthdate }}
                                            @endif
                                        </strong>
                                    </p>
                                    <div class="dropdown ms-auto">
                                        <!-- Card share action menu -->
                                        <a class="nav nav-link text-secondary mb-0" href="#" id="aboutAction2"
                                           data-bs-toggle="dropdown" aria-expanded="false">
                                            <i class="bi bi-three-dots"></i>
                                        </a>
                                        <!-- Card share action dropdown menu -->
                                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="aboutAction2">
                                            <li><a class="dropdown-item small" data-bs-toggle="modal"
                                                   data-bs-target="#update-birthdate-modal"> <i
                                                        class="bi bi-pencil-square fa-fw pe-2"></i>Edit</a></li>
                                        </ul>

                                        {{-- Modal for edit birthdate --}}
                                        <div class="modal fade" id="update-birthdate-modal" tabindex="-1"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Edit about</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                                aria-label="Close"></button>
                                                    </div>
                                                    <form method="POST"
                                                          action="/profile/{{ $currentUser->id }}/update-profile-birthdate"
                                                          class="w-100">
                                                        @method('PUT')
                                                        @csrf
                                                        <div class="modal-body">
                                                            <div class="mb-3 form-group">
                                                    <textarea id="updated-profile-birthdate" name="updated-profile-birthdate"
                                                              class="form-control"> {{ $currentUser->birthdate }}</textarea>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer form-group">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">
                                                                Close
                                                            </button>
                                                            <button type="submit" class="btn btn-primary">Update
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Birthday END -->
                            </div>
                            <div class="col-sm-6">
                                <!-- Status START -->
                                <div class="d-flex align-items-center rounded border px-3 py-2">
                                    <!-- Date -->
                                    <p class="mb-0 text-muted">
                                        <i class="bi bi-heart fa-fw me-2"></i> Status:
                                        <strong>
                                            @if($currentUser->status === "")
                                                Status not set
                                            @else
                                                {{ $currentUser->status }}
                                            @endif
                                        </strong>
                                    </p>
                                    <div class="dropdown ms-auto">
                                        <!-- Card share action menu -->
                                        <a class="nav nav-link text-secondary mb-0" href="#" id="aboutAction3"
                                           data-bs-toggle="dropdown" aria-expanded="false">
                                            <i class="bi bi-three-dots"></i>
                                        </a>
                                        <!-- Card share action dropdown menu -->
                                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="aboutAction3">
                                            <li><a class="dropdown-item" data-bs-toggle="modal"
                                                   data-bs-target="#update-status-modal"> <i
                                                        class="bi bi-pencil-square fa-fw pe-2"></i>Edit</a></li>
                                        </ul>

                                        {{-- Modal for edit birthdate --}}
                                        <div class="modal fade" id="update-status-modal" tabindex="-1"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Edit about</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                                aria-label="Close"></button>
                                                    </div>
                                                    <form method="POST"
                                                          action="/profile/{{ $currentUser->id }}/update-profile-status"
                                                          class="w-100">
                                                        @method('PUT')
                                                        @csrf
                                                        <div class="modal-body">
                                                            <div class="mb-3 form-group">
                                                    <textarea id="updated-profile-status" name="updated-profile-status"
                                                              class="form-control"> {{ $currentUser->status }}</textarea>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer form-group">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">
                                                                Close
                                                            </button>
                                                            <button type="submit" class="btn btn-primary">Update
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Status END -->
                            </div>
                            <div class="col-sm-6">
                                <!-- Lives START -->
                                <div class="d-flex align-items-center rounded border px-3 py-2">
                                    <!-- Date -->
                                    <p class="mb-0 text-muted">
                                        <i class="bi bi-geo-alt fa-fw me-2"></i> Lives in:
                                        <strong>
                                            @if($currentUser->place === "")
                                                Place not set
                                            @else
                                                {{ $currentUser->place }}
                                            @endif
                                        </strong>
                                    </p>
                                    <div class="dropdown ms-auto">
                                        <!-- Card share action menu -->
                                        <a class="nav nav-link text-secondary mb-0" href="#" id="aboutAction5"
                                           data-bs-toggle="dropdown" aria-expanded="false">
                                            <i class="bi bi-three-dots"></i>
                                        </a>
                                        <!-- Card share action dropdown menu -->
                                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="aboutAction5">
                                            <li><a class="dropdown-item" data-bs-toggle="modal"
                                                   data-bs-target="#update-lives-modal"> <i
                                                        class="bi bi-pencil-square fa-fw pe-2 small"></i>Edit</a></li>
                                        </ul>

                                        {{-- Modal for edit lives --}}
                                        <div class="modal fade" id="update-lives-modal" tabindex="-1"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Edit about</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                                aria-label="Close"></button>
                                                    </div>
                                                    <form method="POST"
                                                          action="/profile/{{ $currentUser->id }}/update-profile-place"
                                                          class="w-100">
                                                        @method('PUT')
                                                        @csrf
                                                        <div class="modal-body">
                                                            <div class="mb-3 form-group">
                                                    <textarea id="updated-profile-place" name="updated-profile-place"
                                                              class="form-control"> {{ $currentUser->place }}</textarea>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer form-group">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">
                                                                Close
                                                            </button>
                                                            <button type="submit" class="btn btn-primary">Update
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Lives END -->
                            </div>
                            <div class="col-sm-6">
                                <!-- Joined on START -->
                                <div class="d-flex align-items-center rounded border px-3 py-2">
                                    <!-- Date -->
                                    <p class="mb-0 text-muted">
                                        <i class="bi bi-envelope fa-fw me-2"></i> Email:
                                        <strong>
                                            @if($currentUser->email === "")
                                                Email not set
                                            @else
                                                {{ $currentUser->email }}
                                            @endif
                                        </strong>
                                    </p>
                                    <div class="dropdown ms-auto">
                                        <!-- Card share action menu -->
                                        <a class="nav nav-link text-secondary mb-0" href="#" id="aboutAction7"
                                           data-bs-toggle="dropdown" aria-expanded="false">
                                            <i class="bi bi-three-dots"></i>
                                        </a>
                                        <!-- Card share action dropdown menu -->
                                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="aboutAction7">
                                            <li><a class="dropdown-item" href="#"> <i
                                                        class="bi bi-pencil-square fa-fw pe-2 small"></i>Edit</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Joined on END -->
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            {{-- 2nd column --}}
            <div class="col-lg-3">
                <div class="card bg-white shadow rounded-0 border-0">
                    <div class="p-3">
                        <h5 class="fw-bold">About</h5>
                        <p class="text-muted">
                            @if($currentUser->about === "")
                                About not set
                            @else
                                {{ $currentUser->about }}
                            @endif
                        </p>
                        <p class="mb-1 text-muted">Born:
                            <span class="fw-bold">
                                @if($currentUser->birthdate === "")
                                    Birthdate not set
                                @else
                                    {{ $currentUser->birthdate }}
                                @endif
                            </span></p>
                        <p class="mb-1 text-muted">Status:
                            <span class="fw-bold">
                                @if($currentUser->status === "")
                                    Status not set
                                @else
                                    {{ $currentUser->status }}
                                @endif
                            </span></p>
                        <p class="mb-0 text-muted">Email:
                            <span class="fw-bold">
                                @if($currentUser->email === "")
                                    Email not set
                                @else
                                    {{ $currentUser->email }}
                                @endif
                            </span></p>
                    </div>
                </div>

                <div class="card bg-white shadow rounded-0 border-0 mt-2">
                    <div class="p-3">
                        <h5 class="fw-bold">Friends</h5>
                        <p class="text-muted">Understanding oneself is a journey of exploration and introspection,
                            delving into the intricate layers of one's thoughts, emotions, values, and experiences.</p>
                        <p class="mb-1 text-muted">Born: <span class="fw-bold">January 6, 1994</span></p>
                        <p class="mb-1 text-muted">Status: <span class="fw-bold">Single</span></p>
                        <p class="mb-0 text-muted">Email: <span class="fw-bold"> {{ $currentUser->email }}</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
